from dolfin import *
import numpy as np
import time
import os
import cProfile
import pstats
#import pyprof2calltree
from PIL import Image

from optimisation.image_bc_setup import multiresolution_algorithm
from optimisation.contours import contour_to_vertices

from utils.meshPyramid import meshPyramid
from utils.path import make_sure_path_exists

def main((contour, im1, im2, savdir, t, scale_param, phys_param, fun_param, multiscale_param, segment, plotting, silent)):
    print "Processing time %d" %(t)
    At, ly, ratio = scale_param
    
    # image dimensions in PIXELS
    Ny, Nx = im1.shape

    Ly = 1e0
    Dy = Ly/Ny

    Dx = ratio[0]*Dy
    Lx = Nx*Dx
    
    # normalize the image pixels according to the mean
    fac = 16./np.mean(im1)
    im1 *= fac
    im2 *= fac

    # penalization weights
    alpha = 0.14 # body forces penalization
    beta = 1e-3 # body forces variation penalization
    gamma = 100 # boundary tangential velocity derivative penalization
    zeta = 1. # divergence penalization
    tol = 2e-4 # convergence
    if fun_param!=None:
        alpha *= fun_param[0]
        beta *= fun_param[1]
        gamma *= fun_param[2]
        zeta *= fun_param[3]
        tol *= fun_param[4]
    
    # we need to allow extrapolation when going through the mesh set
    parameters["allow_extrapolation"] = True

    # convert the contour in pixel units to a list of vertices in Point-dolfin-format and length units
    vertices = contour_to_vertices(contour, Dx, Dy)

    # generate the mesh
    resampleVertices=True if segment else False
    if multiscale_param == None:
        multiscale_param = [5, None, 1.5]
    meshes, areas = meshPyramid(vertices, Nx, Ny, Lx, Ly, factor=multiscale_param[2], min_Nx=multiscale_param[0], max_Nx=multiscale_param[1], resampleVertices=resampleVertices) #min_Nx=Nx to get only one finest mesh # min_Nx=20, max_Nx=40, resampleVertices=False

    # run the algorithm
    if not silent:
	   print "Time %d: Meshes generated" %(t)

	   t1 = time.time()
	   pr = cProfile.Profile()
	   pr.enable()
	   u, p, f, g, mesh, I1, I2, gradI2, r = multiresolution_algorithm(meshes, areas, im1, im2, ratio, alpha, beta, gamma, zeta, tol, silent)
	   t2 = time.time()
	   pr.disable()
	   print "BioFlow finished in %.3f s" % (t2-t1)
	   
	   stats = pstats.Stats(pr)
	   stats.strip_dirs().sort_stats('time').print_stats(20)
	   stats.strip_dirs().sort_stats('cumulative').print_stats(20)
	   #pyprof2calltree.convert('cprofile', 'callgrind.cprofile')
    else:
	   t1 = time.time()
	   u, p, f, g, mesh, I1, I2, gradI2, r = multiresolution_algorithm(meshes, areas, im1, im2, ratio, alpha, beta, gamma, zeta, tol, silent)
	   t2 = time.time()
	   print "BioFlow finished in %.3f s" % (t2-t1)


    #### Solution and data handling
    #t=t+298. time offset if videosplit
    # dimensionalize the results
    # x=x^l=x^ly, y=y^l=y^ly, p=p^k=p^eta/At,
    # f=f^q=f^eta/(At*ly), t=t^tref=t^At, u=u^U=u^ly/At
    # r=r^R=r^U/ly=d^/At
    eta = phys_param[0] #Pa.s
    print " Dimesionalizing the results using eta %.2f, time interval %.2f and y image length %.2f" %(eta, At, ly)
    Vel = VectorElement("CG", mesh.ufl_cell(), 2); V = FunctionSpace(mesh, Vel)  # Velocity
    Yor = VectorElement("CG", mesh.ufl_cell(), 1); Y = FunctionSpace(mesh, Yor)  # Force
    Qes = FiniteElement("CG", mesh.ufl_cell(), 1); Q = FunctionSpace(mesh, Qes)  #r0! Out-of-plane flow
    u = project(u*ly/At,V) #um/s
    p = project(p*eta/At,Q) #Pa.s/s   
    f = project(1e3*f*eta/(At*ly),Y) #Pa.s/(s.um) + unit change
    r = project(r/At, Q) #1/s


    # plot the data
    if plotting:
	   print "Plotting"
	   plot(u, title="u", rescale = True)
	#   plot(g, title="g", rescale = True)
	 #  plot(p, title="p", rescale = True)
	  # plot(fmag, title="fmag", rescale = True)
	   interactive()

    print "Saving time %d to %s" %(t, os.path.abspath(savdir))


    # prepare data to be dumped into files
    names = ["u", "p", "f", "g", "mesh", "I1", "I2", "r"]
    dataset = [u, p, f, g, mesh, I1, I2, r]
    # Q = FunctionSpace(mesh, "CG", 1)
    dataset[1]=project(dataset[1], Q)

    u.rename('u','u')
    dataset[1].rename('p','p')
    f.rename('f','f')
    r.rename('r', 'r')

    # t=t+31
    # dump data into files
    for name, data in zip(names, dataset):
	   # VTK/Paraview format
	   pvd_path = os.path.join(savdir, name, "%s_pvd" %(name))
	   make_sure_path_exists(pvd_path)
	   File(os.path.join(pvd_path, "%s_t%03d.pvd" %(name, t))) << data
	   #File(os.path.join(pvd_path, "%s.pvd" %(name))) << (data, t)
	   #Dolfin XML format
	   #dolfin_xml_path = os.path.join(savdir, name, "%s_dolfin-xml" %(name))
	   #make_sure_path_exists(dolfin_xml_path)
	   #File(os.path.join(dolfin_xml_path, "%s_t%03d.xml" %(name, t))) << data

    # max, min, avg # need to interpolate velocity
    Vel1 = FiniteElement("CG", mesh.ufl_cell(), 1); V1 = FunctionSpace(mesh, Vel1)  # project to dim 1
    new_u = project(sqrt(inner(u,u)),V1)
    new_f = project(sqrt(inner(f,f)),V1)
    maxu = new_u.vector().max(); minu = new_u.vector().min(); avgu = new_u.vector().sum()/new_u.vector().size();
    maxf = new_f.vector().max(); minf = new_f.vector().min(); avgf = new_f.vector().sum()/new_f.vector().size();
    maxp = p.vector().max(); minp = p.vector().min(); avgp = p.vector().sum()/p.vector().size();
    data = '%.16f %.16f %.16f %.16f %.16f %.16f %.16f %.16f %.16f %.16f\n' %(t*At, maxu, minu, avgu, maxf, minf, avgf, maxp, minp, avgp) 
    #pvd_path = os.path.join(savdir, "data", "%t%03d_aux" %t))
	#make_sure_path_exists(pvd_path)
    #File(os.path.join(pvd_path, "%t%03d_aux" %t)) << data
    ppd_path = os.path.join(savdir, "data")
    make_sure_path_exists(ppd_path)
    pvpath= os.path.join(ppd_path, "t%03d_aux.txt" %(t))
    file23=open(pvpath, "w"); file23.write(data)

    return t
