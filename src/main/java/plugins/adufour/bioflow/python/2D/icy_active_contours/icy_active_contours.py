from java.awt import Color
from java.awt import EventQueue
from java.lang import Runnable

from icy.main import Icy
from icy.image import IcyBufferedImage
from icy.sequence import Sequence, SequenceUtil
from icy.type import DataType

from plugins.adufour.activecontours import ActiveContours
from plugins.adufour.activecontours import SlidingWindow
from plugins.fab.trackmanager import TrackGroup
from plugins.fab.trackmanager import TrackManager

def activeContours(seq, seg_param=None):
    if seg_param == None:
        seg_param = [1.5, 2., 1E-2]
    # first remove any trackgroup attached to that sequence
    for swimmingObject in Icy.getMainInterface().getSwimmingPool().getObjects(TrackGroup):
        trackGroup = swimmingObject.getObject()
        if (isinstance(trackGroup, TrackGroup)
            and trackGroup.getSequence() == seq):
            Icy.getMainInterface().getSwimmingPool().remove(swimmingObject)

    ac = ActiveContours()

    # the call to createUI and the modifications of the plugin parameters
    # have to be done from the main event thread, so they are encapsulated
    # in a Runnable called with EventQueue.invokeAndWait
    def run():
        ac.createUI()
        ac.input.setValue(seq)

        # regul_weight needs to be adjusted depending on resolution
        ac.regul_weight.setValue(0.05) #0.1)#0.01)#0.05*2*2)
# for roman part 0 = 0.1, for part 1 = 0.05, for part 2 = 0.05
# for beryl 20130703 CytoD08 = 0.1
        
        ac.edge_weight.setValue(0.)
        ac.region_weight.setValue(1.)

        ac.region_sensitivity.setValue(seg_param[0]) #2.273
        
        ac.balloon_weight.setValue(0.)
        ac.axis_weight.setValue(0.)
        ac.coupling_flag.setValue(True)
        ac.evolution_bounds.setNoSequenceSelection()
        ac.contour_resolution.setValue(seg_param[1])#2.)
        #ac.contour_minArea.setValue(10) #unavailable since AC 2.6.0.0
        ac.contour_timeStep.setValue(0.5)#0.1) #0.01)#0.05)
        ac.convergence_winSize.setValue(100) #500)
        ac.convergence_operation.setValue(SlidingWindow.Operation.VAR_COEFF)
        ac.convergence_criterion.setValue(seg_param[2])
        #ac.output_rois.setValue(ac.ExportROI.ON_NEW_IMAGE)
        ac.output_roiType.setValue(ac.ROIType.POLYGON)
        ac.tracking.setValue(True)
        ac.evolution_bounds.getVariable().setValue(None)

        ac.volume_constraint.setValue(True)

        
    EventQueue.invokeAndWait(run)
    
    print "Active contours start"
    ac.execute()
    print "Active contours finished"    

    # cleanup
    ac.getUI().close()

    #width = seq.getWidth()
    #height = seq.getHeight()
    #labelsSequence = Sequence()

    # find the trackGroup produced by ActiveContours
    for swimmingObject in Icy.getMainInterface().getSwimmingPool().getObjects(TrackGroup):
        candidateTrackGroup = swimmingObject.getObject()
        if (isinstance(candidateTrackGroup, TrackGroup)
            and candidateTrackGroup.getSequence() == seq):
            trackGroup = candidateTrackGroup

    trackGroup = ac.getTrackGroup()

    trackSegments = trackGroup.getTrackSegmentList()

    if len(trackSegments) > 1:
        print "Warning! The contour has divided itself during the tracking."
        print "We continue with the first contour (arbitrarily)."

    trackSegment = trackSegments[0]

    contours = []

    times = range(trackSegment.getDetectionList().size())

    # get all the active contours in the image sequence
    for t in times:
        contour = trackSegment.getDetectionAt(t)

        # convert to a standard python list
        pyContour = [[point.x, point.y, point.z] for point in contour.iterator()]
	
        contours += [pyContour]

        # also build a mask sequence
        #labelsImage = contourToLabelImage(contour, width, height)
        #labelsSequence.setImage(t, 0, labelsImage)

    return contours#, labelsSequence

def contourToLabelImage(contour, width, height):      
    labelsIMG = IcyBufferedImage(width, height, 1, DataType.UBYTE)

    g = labelsIMG.createGraphics()
    g.setColor(Color(1))
    g.fill(contour.path)

    return labelsIMG


if __name__ == '__main__':
    seq = Icy.getMainInterface().getFocusedSequence()
    #contours, labelsSequence = activeContours(seq)
    #Icy.addSequence(labelsSequence)
    contours = activeContours(seq)
