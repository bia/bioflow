import execnet
import environment_setup # to setup environment variables that are not propagated on MacOS
import sys

# code heavily inspired from:
# codespeak.net/execnet/example/test_multi.html

#CONCURRENT_PROCESSES = 16

## This thing unpacks the images and sets up all the channels to connect the different platforms

# register remote stdout and stderr channels,
# so that prints and errors are printed in the Icy console
def setup_stdstreams(gateway):
    # send stdout and stderr channels to Icy,
    # so that prints and errors are printed in the Icy console    
    channel = gateway.remote_exec("""
    import sys
		  
    outchan = channel.gateway.newchannel()
    sys.stdout = outchan.makefile("w")
    channel.send(outchan)
    
    errchan = channel.gateway.newchannel()
    sys.stderr = errchan.makefile("w")
    channel.send(errchan)
    """)
    
    # receive remote stdout and stderr as channels
    outchan = channel.receive()
    errchan = channel.receive()

    # associate the channels with printing callbacks
    # note: callbacks execute in receiver thread
    outchan.setcallback(lambda data: sys.stdout.write(str(data)))
    errchan.setcallback(lambda data: sys.stdout.write(str(data)))

    # wait for the remote script to be finished
    channel.waitclose()

def process_item(channel):
    from icy_optimisation_remote import meshsetup__resultdumping
    #from numpyexecnet import unpack_image
    import numpy as np
    
    # task processor, sits on each CPU
    channel.send("ready")
    for x in channel:
        if x is None: # we can shutdown
            print "gw shutting down"
            break

        print "got a task"
        [contour, images, savdir, t, scale_param, phys_param, fun_param, multiscale_param, segment, plotting, silent] = x
        unpacked_images = [np.array(im[2]).astype(float).reshape(im[0],im[1]) for im in images]
	#unpacked_images = [np.flipud(np.array(im[2]).reshape(im[0],im[1])) for im in images]
	#contour = [[x, images[0][0]-1-y, z] for [x, y, z] in contour][::-1]
	#activate to flip the image
        if True:
            args = [contour, unpacked_images[0], unpacked_images[1], savdir, t, scale_param, phys_param, fun_param, multiscale_param, segment, plotting, silent]
            meshsetup__resultdumping.main(args) 

        channel.send("done")

def process_tasks_dolfin(tasks, parallel=None):
	from utils.icyexecnetgateway import pack_image, IcyExecnetGateway
	environment_setup.setup_environment()
	python_path = environment_setup.get_python_path()
	add_paths_code = environment_setup.get_remote_addpaths_code()

	if parallel==None:
		with IcyExecnetGateway(python_path) as gw:
			gw.remote_exec(add_paths_code)
			gw.remote_exec(process_item)
			for task in tasks:
				gw.send(task)
			gw.send(None)
		return
	
	CONCURRENT_PROCESSES = int(parallel)
	
	print "Creating Group"
	
	group = execnet.Group()
	group.defaultspec = "popen//python=" + python_path

	nThreads = min(CONCURRENT_PROCESSES, len(tasks))
	print str(len(tasks)) + " tasks to process"
	print "Creating " + str(nThreads) + " parallel threads"
	for i in range(nThreads):
		group.makegateway()

	print "Redirecting streams for each thread to Icy"
	for gw in group:
		setup_stdstreams(gw)

	print "Setting up paths"
	mch = group.remote_exec(add_paths_code)
	mch.waitclose()
	
	# execute the same code everywhere
	mch = group.remote_exec(process_item)
	
	# get a queue that gives us results
	q = mch.make_receive_queue(endmarker=-1)

	iteration = 0
	
	terminated = 0
	while 1:
		# print "Running iteration #" + str(iteration)
		iteration += 1

		print "Waiting for the next available thread"
		channel, item = q.get()

		print "Thread " + str(channel.gateway.id) + " returned " + str(item)
		
		if item == -1:
			print "Thread " + str(channel.gateway.id) + " has terminated"
			terminated += 1
			print "Number of terminated threads: " + str(terminated)
			if terminated == len(mch):
				print "got all results, terminating"
				break
			# print "Not all threads have terminated => looping"
			continue
		if item != "ready":
			print "Thread %s returned %r" % (channel.gateway.id, item)
		if not tasks:
			print "No tasks remain, sending termination request to all"
			mch.send_each(None)
			tasks = -1
		if tasks and tasks != -1:
			# print "Retrieving a new task"
			task = tasks.pop()
			print "Sending task to thread %s" % (channel.gateway.id)
			channel.send(task)
	
	# waitclose will re-raise any exception that occured in the remotes
	mch.waitclose()
	# robust termination
	group.terminate()

def process_tasks_docker():
	import pickle
	import os
	import execnet
	import time
	import sys
	t0 = time.time()
	inner_docker_path = "/home/fenics/shared"
	sys.path.append(inner_docker_path + '/python/2D')
	environment_setup.setup_environment()
	python_path = environment_setup.get_python_path()
	add_paths_code = environment_setup.get_remote_addpaths_code()
	from utils.path import make_sure_path_exists
	from utils.pvdWriter import writePvdFile
	from utils.icyexecnetgateway import pack_image, IcyExecnetGateway

	pickle_file = open(os.path.join(inner_docker_path, "pickle_rick.pkl"), "rb")
	parallel, n_pairs = pickle.load(pickle_file)
	tasks = [1.]*n_pairs

	if parallel==None:
		with IcyExecnetGateway(python_path) as gw:
			gw.remote_exec(add_paths_code)
			gw.remote_exec(process_item)
			for task in tasks:
				gw.send(pickle.load(pickle_file))
			gw.send(None)
		return
	
	CONCURRENT_PROCESSES = int(parallel)
	
	print "Creating Group"
	
	group = execnet.Group()
	group.defaultspec = "popen//python=" + python_path

	nThreads = min(CONCURRENT_PROCESSES, len(tasks))
	print str(len(tasks)) + " tasks to process"
	print "Creating " + str(nThreads) + " parallel threads"
	for i in range(nThreads):
		group.makegateway()

	print "Redirecting streams for each thread to Icy"
	for gw in group:
		setup_stdstreams(gw)

	print "Setting up paths"
	mch = group.remote_exec(add_paths_code)
	mch.waitclose()
	
	# execute the same code everywhere
	mch = group.remote_exec(process_item)
	
	# get a queue that gives us results
	q = mch.make_receive_queue(endmarker=-1)

	iteration = 0
	
	terminated = 0
	while 1:
		# print "Running iteration #" + str(iteration)
		iteration += 1

		print "Waiting for the next available thread"
		channel, item = q.get()

		print "Thread " + str(channel.gateway.id) + " returned " + str(item)
		
		if item == -1:
			print "Thread " + str(channel.gateway.id) + " has terminated"
			terminated += 1
			print "Number of terminated threads: " + str(terminated)
			if terminated == len(mch):
				print "got all results, terminating"
				break
			# print "Not all threads have terminated => looping"
			continue
		if item != "ready":
			print "Thread %s returned %r" % (channel.gateway.id, item)
		if not tasks:
			print "No tasks remain, sending termination request to all"
			mch.send_each(None)
			tasks = -1
		if tasks and tasks != -1:
			# print "Retrieving a new task"
			tasks.pop()
			task = pickle.load(pickle_file)
			print "Sending task to thread %s" % (channel.gateway.id)
			channel.send(task)
	
	# waitclose will re-raise any exception that occured in the remotes
	mch.waitclose()
	# robust termination
	group.terminate()
	pickle_file.close()
	print "Writing global pvd file"
	names = ["u", "p", "f", "g", "mesh", "I1", "I2", "r"]
	times = range(n_pairs)
	for name in names:
		pvd_path = os.path.join(inner_docker_path, "output", name, "%s_pvd" %(name))
		make_sure_path_exists(pvd_path)
		filename = os.path.join(pvd_path, "%s.pvd" %(name))
		writePvdFile(filename, name, times)
	t1 = time.time()
	print "Total execution time = %d s, for %d pairs of images, %d s per pair" %(t1-t0, n_pairs, (t1-t0)/n_pairs)
	return

if __name__ == "__main__":
	process_tasks_docker()
	exit()
	