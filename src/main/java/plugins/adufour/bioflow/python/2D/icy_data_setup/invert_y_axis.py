from icy.main import Icy
from icy.type import DataType
from icy.type.collection.array import ArrayUtil, Array1DUtil

    
def y_inverter(s):
    temp = ArrayUtil.createArray(s.getDataType_(), 1, s.getSizeX())
    for t in range(s.getSizeT()):
	    for i in range(s.getSizeY() / 2):
		    Array1DUtil.arrayToArray(s.getDataXY(t,0,0), i*s.getSizeX(), temp, 0, s.getSizeX(), False)
		    Array1DUtil.arrayToArray(s.getDataXY(t,0,0), (s.getSizeY() - (i+1)) *s.getSizeX(), s.getDataXY(t,0,0), i*s.getSizeX(), s.getSizeX(), False)
		    Array1DUtil.arrayToArray(temp, 0, s.getDataXY(t,0,0), (s.getSizeY() - (i+1)) *s.getSizeX(),s.getSizeX(), False)
    return s
