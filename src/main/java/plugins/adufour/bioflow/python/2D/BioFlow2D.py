import sys, os, inspect
import datetime

# modify the python path to find sub-scripts
sys.path.append(os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None))))

from icy.main import Icy
from icy_data_setup.datagather import optimizer
# main parameters
# (note that additional system-specific parameters (paths, etc) are defined in icy_environment_setup.py)

seq = Icy.getMainInterface().getFocusedSequence()
savdir = "~/Documents/BioFlow/%s_%s_%dx%d" %(datetime.datetime.now().strftime("%Y-%m-%d_%Hh%M"), str(seq.getName()), seq.getSizeX(), seq.getSizeY())
parallel = 12 # run the algorithms on multiple image pairs at the same time
plotting = True # Fenics plots

phys_param = [1.] # viscosity constant in Pa.s
fun_param = None # functional constants/penalizations:
                        # [body_forces, 1., tangential_velocity, out_of_plane, tolerance(w.r. to 1e-4)]
                        # [1., 1., 1., 1., 1.] is default
scale_param = None # image sequence parameters if not in metadata:
                   # [time_between_frames, y_size, [pixel_size_x/pixel_size_y]]
                   # in seconds, microns, unitless ratio
segment = True

segmentation_param = None # segmentation sensitivity, resolution and convergence:
                # [region_sensitivity, contour_resolution, convergence_criterion]
                # [1.5, 2., 1E-2] is default
multiscale_param = None # multi_scale analysis
                        # min_pixel_size, max_pixel_size, scale_factor
                        # [ 5, None, 1.5] is default

# launch the algorithm
optimizer(seq, savdir, parallel, segment, plotting, phys_param, fun_param, scale_param, segmentation_param, multiscale_param)
