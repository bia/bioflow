from dolfin import *
from scipy.misc import toimage
import numpy as np
import math

from utils.imageDerivatives import image_first_derivatives
from utils.dolfin_numpy_utils import array_to_scalar, arrays_to_vector
from utils.warp import warp, outside_indices, warp_coords
from utils.pyramid import downscale_single, downsample_single
from stokessolverOOP import flsolver

def multiresolution_algorithm(meshes, areas, im1, im2, ratio, alpha, beta, gamma, tol, silent=True, ug0=None):

    # number of pixels
    Ny, Nx = im1[0].shape
    Nz = len(im1)
    # length
    Ly = 1e0
    Dy = Ly/Ny
    
    Dx = ratio[0]*Dy
    Lx = Nx*Dx
    Dz = ratio[1]*Dy
    Lz = Nz*Dz
    
    for i, (mesh, area) in enumerate(zip(meshes[::-1], areas[::-1])):

        n = mesh.num_cells()
        print "Scale %d/%d (%d cells, corresponding 3Dimage size approx. = %dx%dx%d)" %(i+1, len(meshes), n, math.pow(n/6., float(1)/3), math.pow(n/6., float(1)/3), math.pow(n/6., float(1)/3))
        
        downscale_factor = math.pow(6.*area/(Dx*Dy*Dz), float(1)/3)
        if not silent:
            print "Downscale factor:", downscale_factor
            
        if False:

            im1_down = downsample_single(im1, downscale_factor)
            im2_down = downsample_single(im2, downscale_factor)
            Ny_H, Nx_H = im1_down[0].shape
            Nz_H = len(im1_down)
            Dx_H = Lx/Nx_H
            Dy_H = Ly/Ny_H
            Dz_H = Lz/Nz_H
            print "Image downsampled"
            
            dxI2, dyI2, dzI2 = image_first_derivatives(im2_down)
            dxI2 = dxI2/Dx_H
            dyI2 = dyI2/Dy_H
            dzI2 = dzI2/Dz_H
            print "Derivatives computed"

            im_mesh = BoxMesh(Point(0., 0., 0.), Point(Lx, Ly, Lz), Nx_H, Ny_H, Nz_H)
            #print 'image mesh created'

            if mesh<>meshes[-1]:

                coords, x_warp, y_warp, z_warp = warp_coords(im_mesh, Nx_H, Ny_H, Nz_H, u, Lx, Ly, Lz, coord=True, silent=silent)
                im2_down = warp(im2_down, coords)
                dxI2 = warp(dxI2, coords)
                dyI2 = warp(dyI2, coords)
                dzI2 = warp(dzI2, coords)
                
                # where warping outside image domain: cancel data-attachment term
                out_ind = outside_indices(x_warp, y_warp, z_warp, Nx_H, Ny_H, Nz_H)
                dxI2[out_ind] = 0.
                dyI2[out_ind] = 0.
                dzI2[out_ind] = 0.
                im2_down[out_ind] = im1_down[out_ind] # so that It = 0
                print "Image warped"

        else:

            im1_down = downscale_single(im1, downscale_factor)
            im2_down = downscale_single(im2, downscale_factor)
            print "Image downscaled"
            
            dxI2, dyI2, dzI2 = image_first_derivatives(im2_down)
            dxI2 = dxI2/Dx
            dyI2 = dyI2/Dy
            dzI2 = dzI2/Dz
            print "Derivatives computed"

            im_mesh = BoxMesh(Point(0., 0., 0.), Point(Lx, Ly, Lz), Nx, Ny, Nz)
            #print 'image mesh created'

            if mesh<>meshes[-1]:

                coords, x_warp, y_warp, z_warp = warp_coords(im_mesh, Nx, Ny, Nz, u, Lx, Ly, Lz, coord=True, silent=silent)
                im2_down = warp(im2_down, coords)
                dxI2 = warp(dxI2, coords)
                dyI2 = warp(dyI2, coords)
                dzI2 = warp(dzI2, coords)
                
                # warp outside image domain: cancel data-attachment term
                out_ind = outside_indices(x_warp, y_warp, z_warp, Nx, Ny, Nz)
                dxI2[out_ind] = 0.
                dyI2[out_ind] = 0.
                dzI2[out_ind] = 0.
                # so It = 0
                im2_down[out_ind] = im1_down[out_ind]
                print "Image warped"


        # convert the image represented in a matrix to a 0 degree function in dolfin (1 ct value per element)
        I1 = array_to_scalar(im_mesh, im1_down)
        I2 = array_to_scalar(im_mesh, im2_down)
        gradI2 = arrays_to_vector(im_mesh, dxI2, dyI2, dzI2)
        
        # PROJECT the images on the image rectangle mesh to the cell mesh
        IFSel = FiniteElement("CG", mesh.ufl_cell(), 1); IFS = FunctionSpace(mesh, IFSel)
        IGFSel = VectorElement("CG", mesh.ufl_cell(), 1); IGFS = FunctionSpace(mesh, IGFSel)
        I1 = project(I1, IFS)
        I2 = project(I2, IFS)
        gradI2 = project(gradI2, IGFS)
        print "Image and Image_grad projected"

        # scale regularisation parameters
        alpha_s = alpha*downscale_factor
        beta_s = beta*downscale_factor
        gamma_s = gamma*downscale_factor
      
        # project previous guess to new guess
        Vel = VectorElement("CG", mesh.ufl_cell(), 2); V = FunctionSpace(mesh, Vel)  # Velocity
        Yor = VectorElement("CG", mesh.ufl_cell(), 1); Y = FunctionSpace(mesh, Yor)  # Force
        if mesh==meshes[-1]:

            f0 = Function(Y)
            uw0 = Function(V)
            g0 = Function(V)
            #f0.vector()[:] = 0. # functions already initialized to 0
            #uw0.vector()[:] = 0.
            #g0.vector()[:] = 0.
            print "Initialized guess" 
       
        else:

            f0 = project(f, Y)
            uw0 = project(u, V)
            g0 = project(g, V)
            print "Projected previous guess"   

        ### run solver algorithm
        u, p, g, f = flsolver(mesh, I1, I2, gradI2, f0, uw0, g0, alpha_s, beta_s, gamma_s, tol=tol)
        print 'Solved'  
    #plot(u, interactive=True)      
    
    return u, p, f, g, mesh, inner(f,f), I1, I2, gradI2

