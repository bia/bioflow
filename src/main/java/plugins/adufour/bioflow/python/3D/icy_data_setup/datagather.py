import sys
import time
import datetime
import os
import errno
import pickle

from plugins.adufour.bioflow import BioFlow #JPD
from icy_active_contours.icy_active_contours import activeContours
from icy_active_contours.icy_make_a_box import make_a_cuboid
from utils.path import make_sure_path_exists
from utils.pvdWriter import writePvdFile

from icy.sequence import SequenceUtil
from icy.type import DataType
from icy.main import Icy
from icyexecnetgateway import pack_image

import environment_setup # to setup environment variables that are not propagated on MacOS
from icy_data_setup.communication_setup import process_tasks_dolfin, process_tasks_docker

# The module checks for proper format, gets the active contours, sets up the images on a list and builds the folder path
# Also, runs the algorithm
def optimizer(seq, savdir=None, parallel=None, segment=True, plotting=True, phys_param=None, fun_param=None, scale_param=None, segmentation_param=None, multiscale_param=None, silent=True):

    t0 = time.time()

    if seq==None:
		 print "No image opened"
		 return

    start = 0; stop = seq.getSizeT()
    print "Time points to be processed: " + str(seq.getSizeT() - 1)
   
    # get the images to double pixel/matrix values
    if seq.getSizeT()<2:
		 print "A sequence with at least two images is needed"
		 return
    
    # functional parameters
    # a, b, g, z, to = 1., 1., 1., 1., 1.
    #fun_param = None # [a, b, g, z, to]

    # physical parameters
    if phys_param == None:
        eta = 1.
        phys_param = [eta]

    # extracting metadata from image
    At = seq.getTimeInterval() #in seconds 
    py = seq.getPixelSizeY()   #in microns
    Hy = seq.getSizeY()        #number of pixels
    zy_ratio = seq.getPixelSizeZ()/seq.getPixelSizeY()
    xy_ratio = seq.getPixelSizeX()/seq.getPixelSizeY()
    ratio = [xy_ratio, zy_ratio]
    ly = py*Hy    
    if At == 0.:
         At = 0.1
         print "Image time interval cannot be found in the metadata, setting it to a 0.1s"
    else:
         print "Image time interval found in the metadata: %.2f s " %At
    if py==1.: # FIX need to REcheck the value Icy returns when data is not found
         print "Image y pixel size either cannot be found in the metadata or its 1 um, setting it to 1 um. Total y size is %.2f um" %ly
    else:
         print "Image y pixel size found in the metadata: %.2f um. Total y size is %.2f um" %(py, ly)
    
    print "xy_ratio: %.2f, zy_ratio: %.2f" %(xy_ratio, zy_ratio)
        
    print "Rescaling the data to [0-1]..."

    if seq.getDataType_() <> DataType.DOUBLE:
		 doubleSeq = SequenceUtil.convertToType(seq, DataType.DOUBLE, False)
    else: doubleSeq = seq
    
    # scale parameters
    if scale_param == None:
        scale_param = [At, ly, ratio]

    # create folder path to store the results
    if savdir == None:
	   savdir = "~/BioFlow/" + datetime.datetime.now().strftime("%Y-%m-%d_%Hh%M")
    savdir = os.path.expanduser(savdir)

    print "Tracking the cell..."

    if not BioFlow.isDolfinInstalled(): #JPD
        original_savdir = savdir #JPD
        savdir = os.path.join(BioFlow.getBaseFolder(), "output") #JPD

    seq.getFirstViewer().setPositionT(start)
    if segmentation_param == None:
        segmentation_param = [4.7, 2.1, 2E-2]
    if segment:
        contours, facets = activeContours(seq, savdir, segmentation_param)
    else:
        contours, facets = make_a_cuboid(seq, savdir)

    if not BioFlow.isDolfinInstalled(): #JPD
        savdir = BioFlow.getBaseFolder() #JPD

    # creates a list of the images
    times = range(start, stop-1)
    images=[[seq.getImage(t, z, 0) for z in range(0, seq.getSizeZ())] for t in range(0, seq.getSizeT())]
    
    # pack 3D images to process
    packed_images = []
    for z_stack in images:
        z_pack = []
        for image in z_stack:
            z_pack += [[image.getSizeY(), image.getSizeX(),
                        image.getDataXY(0).tolist()]]
        packed_images.append(z_pack)


    print "Launching the analysis (" + str(len(packed_images) - 1) + " tasks)"

    # gathers all the data in image-pairs sets to be analyzed separately
    #if os.system("dolfin-version > /dev/null 2>&1")==0: #JPD
    if BioFlow.isDolfinInstalled():
        print("Dolfin detected: using system dolfin")
        args = []
        if True:
            for contour, facet, image0, image1, t in zip(contours[:-1], facets[:-1], packed_images[:-1], packed_images[1:], times):
		#for contour, facet, image0, image1, t in zip(contours, facets, packed_images[::-1][1:], packed_images[::-1][:-1], times):
                args.append([contour, facet, [image0, image1], savdir, t, scale_param, phys_param, fun_param, multiscale_param, segment, plotting, silent])

        # Process all the sequence by image pairs
        process_tasks_dolfin(args, parallel=parallel) # to run all the sequence by pairs
        #process_tasks([args[0]], parallel=parallel) # to run pair number [0]

    #elif os.system("docker --version > /dev/null 2>&1")==0: #JPD
    else:
        print("No dolfin detected: using system docker")
        pickle_place = os.path.join(savdir, "pickle_rick.pkl")
        #make_sure_path_exists(savdir)  ==> Seems to cause some strange "Errno 20000" intermittently, and in fact is not needed... 
        picky = open(pickle_place, 'wb')
        pickle.dump([parallel, len(times)], picky, -1)
        if True:
            for contour, facet, image0, image1, t in zip(contours[:-1], facets[:-1], packed_images[:-1], packed_images[1:], times):
                pickle.dump([contour, facet, [image0, image1], "/home/fenics/shared/output", t, scale_param, phys_param, fun_param, multiscale_param, segment, plotting, silent], picky, -1) #JPD

        picky.close()
        # Process all the sequence by image pairs
        #process_tasks_docker()
        BioFlow.runViaDocker3D(original_savdir) #JPD
        return

    #else: #JPD
        #print("No dolfin, no docker: exiting")
        #return 1


    # global pvd files
    print "Writing global pvd file"
    names = ["u", "p", "f", "g", "mesh", "fmag", "I1", "I2"]
    for name in names:
        pvd_path = os.path.join(savdir, name, "%s_pvd" %(name))
        make_sure_path_exists(pvd_path)
        filename = os.path.join(pvd_path, "%s.pvd" %(name))
        writePvdFile(filename, name, times)
  
    t1 = time.time()
    print "Total execution time = %d s, for %d pairs of images, %d s per pair" %(t1-t0, len(times), (t1-t0)/len(times))
    return 1
