from dolfin import *
from mshr import *
import numpy as np
import math
import time
import os
import cProfile
import pstats
#import pyprof2calltree
from PIL import Image

from optimisation.multiresolution_imagetarget_warp import multiresolution_algorithm
from optimisation.contours import contour_to_vertices
from optimisation.subdomain_projection import project_inside, vector_project_inside

from utils.dolfin_numpy_utils import array_to_scalar
from utils.meshPyramid import meshPyramid
from utils.path import make_sure_path_exists

def main((contour, facet, im1, im2, savdir, t, scale_param, phys_param, fun_param, multiscale_param, segment, plotting, silent)):
    At, ly, ratio = scale_param    

    ## number of pixels
    yxz_imsize = im1[0].shape
    yxz_imsize += (len(im1),) 
    Ny, Nx, Nz = yxz_imsize
    ## length
    Ly = 1e0
    Dy = Ly/Ny
    
    Dx = ratio[0]*Dy
    Lx = Nx*Dx
    Dz = ratio[1]*Dy
    print ratio[0], ratio[1], 2.5625
    Lz = Nz*Dz   
 
    if not silent:
	   print yxz_imsize, im1[0].dtype, "Lx", Lx, "Ly", Ly, "Lz", Lz

    # normalize the image pixels
    fac = 16./np.mean(im1)
    for i in range(0, yxz_imsize[2]): 
    	im1[i] = im1[i]/np.mean(im1[i])
    	im2[i] = im2[i]/np.mean(im2[i])

    # penalization weights
    alpha = 0.14 # body forces penalization
    beta = 1e-3 # body forces variation penalization
    gamma = 10 # boundary tangential velocity derivative penalization
    tol = 2e-4
    if fun_param!=None:
        alpha *= fun_param[0]
        beta *= fun_param[1]
        gamma *= fun_param[2]
        tol *= fun_param[4]
    
    # we need to allow extrapolation when going through the mesh set
    parameters["allow_extrapolation"] = True

    ### Generate one mesh from the .off file
    resampleVertices=True if segment else False
    print 'Starting the meshing'
    flname = os.path.join(savdir, "off_meshes", "%dverfa.off" %(t))
    #flname = "%dverfa.off" %(t)
    if multiscale_param == None:
        ctro = np.array(contour)
        bounding_box=max(ctro[:,0].ptp(), ctro[:,1].ptp(), ctro[:,2].ptp())
        mesh_r = bounding_box/(np.sqrt(Dx**2 + Dy**2 + Dz**2))
        multiscale_param = [5, None, 1.5, mesh_r]

    meshes, areas = meshPyramid(Nx, Ny, Nz, Lx, Ly, Lz, flname, factor=multiscale_param[2], min_Nx=multiscale_param[0], max_Nx=multiscale_param[1], mesh_resolution=multiscale_param[3], resampleVertices = resampleVertices)
    print 'Done with the meshing'

    # run the algorithm
    if not silent:
	   print "Meshes generated"

	   t1 = time.time()
	   pr = cProfile.Profile()
	   pr.enable()
	   u, p, f, g, mesh, fmag, I1, I2, gradI2 = multiresolution_algorithm(meshes, areas, im1, im2, ratio, alpha, beta, gamma, tol, silent) 
	   t2 = time.time()
	   pr.disable()
	   print "BioFlow finished in %.3f s" % (t2-t1)
	   
	   stats = pstats.Stats(pr)
	   stats.strip_dirs().sort_stats('time').print_stats(20)
	   stats.strip_dirs().sort_stats('cumulative').print_stats(20)
	   #pyprof2calltree.convert('cprofile', 'callgrind.cprofile')
    else:
	   t1 = time.time()
	   u, p, f, g, mesh, fmag, I1, I2, gradI2 = multiresolution_algorithm(meshes, areas, im1, im2, ratio, alpha, beta, gamma, tol, silent)
	   t2 = time.time()
	   print "BioFlow finished in %.3f s" % (t2-t1)

    #### Solution and data handling
    #t=t+298. time offset if videosplit
    # dimensionalize the results
    # x=x^l=x^ly, y=y^l=y^ly, p=p^k=p^eta/At,
    # f=f^q=f^eta/(At*ly), t=t^tref=t^At, u=u^U=u^ly/At
    # r=r^R=r^U/ly=d^/At
    eta = phys_param[0] #1. #Pa.s
    print " Dimesionalizing the results using eta %.2f, time interval %.2f and y image length %.2f" %(eta, At, ly)
    Vel = VectorElement("CG", mesh.ufl_cell(), 2); V = FunctionSpace(mesh, Vel)  # Velocity
    Yor = VectorElement("CG", mesh.ufl_cell(), 1); Y = FunctionSpace(mesh, Yor)  # Force
    Qes = FiniteElement("CG", mesh.ufl_cell(), 1); Q = FunctionSpace(mesh, Qes)  #r0! Out-of-plane flow
    u = project(u*ly/At,V) #um/s
    p = project(p*eta/At,Q) #Pa.s/s   
    f = project(1e-3*f*eta/(At*ly),Y) #Pa.s/(s.um) + unit change 2D: 1e3, 3D:1e-3
    
    # plot the data
    if plotting:
        print "Plotting"
        plot(u, title="u", interactive = True)
     #   plot(g, title="g", rescale = True)
      #  plot(p, title="p", rescale = True)
       # plot(fmag, title="fmag", rescale = True)
 
    print "Saving time %d to %s" %(t, os.path.abspath(savdir))

    # prepare data to be dumped into files
    names = ["u", "p", "f", "g", "mesh", "fmag", "I1", "I2"]
    dataset = [u, p, f, g, mesh, fmag, I1, I2]
    #Q = FunctionSpace(mesh, "CG", 1)
    dataset[1]=project(dataset[1], Q)
    dataset[5]=project(dataset[5], Q)

    print "printing to vtu, pvd"
    # dump solutions to files
    for name, data in zip(names, dataset):
	   # VTK/Paraview format
	   pvd_path = os.path.join(savdir, name, "%s_pvd" %(name))
	   make_sure_path_exists(pvd_path)
	   File(os.path.join(pvd_path, "%s_t%03d.pvd" %(name, t))) << data
	   # Dolfin XML format
	   #dolfin_xml_path = os.path.join(savdir, name, "%s_dolfin-xml" %(name))	   
	   #make_sure_path_exists(dolfin_xml_path)	   
	   #File(os.path.join(dolfin_xml_path, "%s_t%03d.xml" %(name, t))) << data

    # max, min, avg # need to interpolate velocity
    Vel1 = FiniteElement("CG", mesh.ufl_cell(), 1); V1 = FunctionSpace(mesh, Vel1)  # project to dim 1
    new_u = project(sqrt(inner(u,u)),V1)
    new_f = project(sqrt(inner(f,f)),V1)
    maxu = new_u.vector().max(); minu = new_u.vector().min(); avgu = new_u.vector().sum()/new_u.vector().size();
    maxf = new_f.vector().max(); minf = new_f.vector().min(); avgf = new_f.vector().sum()/new_f.vector().size();
    maxp = p.vector().max(); minp = p.vector().min(); avgp = p.vector().sum()/p.vector().size();
    data = '%.16f %.16f %.16f %.16f %.16f %.16f %.16f %.16f %.16f %.16f\n' %(t*At, maxu, minu, avgu, maxf, minf, avgf, maxp, minp, avgp) 
    #pvd_path = os.path.join(savdir, "data", "%t%03d_aux" %t))
	#make_sure_path_exists(pvd_path)
    #File(os.path.join(pvd_path, "%t%03d_aux" %t)) << data
    ppd_path = os.path.join(savdir, "data")
    make_sure_path_exists(ppd_path)
    pvpath= os.path.join(ppd_path, "t%03d_aux.txt" %(t))
    file23=open(pvpath, "w"); file23.write(data)

    return t
