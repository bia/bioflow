if __name__ == '__channelexec__':

	print "Hello from remote :)"

	import numpyexecnet
	from multiprocessing import Pool
	from icy_optimisation_remote import single

	params = channel.receive()

	print "Received the params list"

	[silent, contours, packed_images, savdir, times, parallel] = params

	images = [numpyexecnet.unpack_image(im) for im in packed_images]

	print "Received silent=", silent
	print "Received %d contours" %(len(contours))
	print "Received %d images" %(len(images))
	print "Received savdir=", savdir
	print "Received times=", times
	print "Received parallel=", parallel

	CONCURRENT_PROCESSES = 12

	# prepare a list of arguments for the worker processes
	args = []
	for contour, image0, image1, t in zip(contours, images[:-1], images[1:], times):
		args.append([contour, image0, image1, savdir, t, silent])

	if len(args) > 1 and parallel:
		# create a pool of subprocesses
		pool = Pool(processes=CONCURRENT_PROCESSES)
	
		print "Process pool created"
	
		# start the worker processes
		ret = pool.map(single.main, args)
	
		print "pool.map returned:", ret
	else:
		for arg in args:
			single.main(arg)
			print "One done."

	print "All done."
