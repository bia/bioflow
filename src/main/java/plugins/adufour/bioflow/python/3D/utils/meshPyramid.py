import numpy as np
import math
from dolfin import *
from mshr import *
#from optimisation.contours import resample
def meshPyramid(Nx, Ny, Nz, Lx, Ly, Lz, flname, factor=1.5, min_Nx=5, max_Nx=None, mesh_resolution=15., resampleVertices=True): ##vertices, facet, 

    Nx_H, Ny_H, Nz_H = Nx, Ny, Nz 
    #flname = "/home/fenics/Desktop/Icy/%dverfa.off" %(t)
    #print flname
    if max_Nx:
        # do not compute on finest mesh anyway
        if Nx_H>max_Nx:
            Ny_H = Ny_H*max_Nx/Nx_H
            Nz_H = Nz_H*max_Nx/Nx_H
            Nx_H = max_Nx
            

    meshes = []
    areas = []

    while Nx_H >= min_Nx and Ny_H >= min_Nx and Nz_H >= min_Nx:
        Dx_H = Lx/Nx_H
        Dy_H = Ly/Ny_H
        Dz_H = Lz/Nz_H
        area = Dx_H*Dy_H*Dz_H/6.
        mesh = Mesh()
        
        cell_size = np.sqrt(Dx_H**2 + Dy_H**2 + Dz_H**2)

        geo = Surface3D(flname)
        geoo = CSGCGALDomain3D(geo)
        if resampleVertices: geoo = geoo.remesh_surface(cell_size, 0.)
        generator = CSGCGALMeshGenerator3D()
        generator.parameters['mesh_resolution'] = mesh_resolution
        #generator.parameters['edge_size'] = cell_size
        #generator.parameters['facet_angle'] = 30.
        #generator.parameters['facet_size'] = cell_size
        #generator.parameters['facet_distance'] = cell_size/10.
        #generator.parameters['cell_radius_edge_ratio'] = 3.
        #generator.parameters['cell_size'] = cell_size
        mesh = generator.generate(geoo)

        #plot(mesh, interactive=True)
        meshes += [mesh]
        areas += [area]

        Ny_H, Nx_H, Nz_H, mesh_resolution = Ny_H/factor, Nx_H/factor, Nz_H/factor, mesh_resolution/factor

    print "=> successfully generated mesh pyramid"
    return meshes, areas

if __name__ == '__main__':
    meshPyramid(77, 83, 12, 0.927710843373, 1.0, 0.370481927711, 0, factor=1.5, min_Nx=40, max_Nx=None, resampleVertices=True)
#77 83 12 0.927710843373 1.0 0.370481927711 0
#bounding_box=max(im_mesh.coordinates()[:,0].ptp(),im_mesh.coordinates()[:,1].ptp(), im_mesh.coordinates()[:,2].ptp())
